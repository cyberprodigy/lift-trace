import { Exercise } from "./dto/Exercise";
import { WorkoutId } from "./WorkoutModel";
import { ObjectUtils } from "../utils/ObjectUtils";

export class Workout {
  private WorkoutId: WorkoutId;
  private CurrentExcerciseIdx = 0;
  private Exercises: Array<Exercise> = [];

  constructor(workoutId: WorkoutId) {
    this.WorkoutId = workoutId;
  }

  public addExcercise(
    id: number,
    name: string,
    sets: string,
    rest: number,
    reps: string,
    weight: number
  ): void {
    if (this.Exercises[id]) {
      throw new Error(`Can not insert ${name} id:${id} already exists`);
    }
    var exercise = new Exercise();
    exercise.id = id;
    exercise.name = name;
    exercise.sets = sets;
    exercise.rest = rest;
    exercise.reps = reps;
    exercise.weight = weight;
    this.Exercises[id] = exercise;
  }

  public getExcercise(id: number) {
    return this.Exercises[id];
  }

  public getWorkoutId(): WorkoutId {
    return this.WorkoutId;
  }

  public getCurrentExcercise() {
    return this.getExcercise(this.CurrentExcerciseIdx);
  }

  public getNextExcercise() {
    this.CurrentExcerciseIdx =
      (this.CurrentExcerciseIdx + 1) % this.Exercises.length;
    return this.getExcercise(this.CurrentExcerciseIdx);
  }

  public getPreviousExcercise() {
    this.CurrentExcerciseIdx = this.CurrentExcerciseIdx - 1;
    if (this.CurrentExcerciseIdx < 0) {
      this.CurrentExcerciseIdx = this.Exercises.length - 1;
    }
    return this.getExcercise(this.CurrentExcerciseIdx);
  }

  public setCurrentExcerciseId(currentExcerciseIdx: number): void {
    this.CurrentExcerciseIdx = currentExcerciseIdx;
  }

  public getCurrentExcerciseId(): number {
    return this.CurrentExcerciseIdx;
  }

  public static fromJSON(data: Object): Workout {
    // Looks like Exercises needs to fromJson function. But feel free to add it
    var newWorkout: Workout = new Workout(WorkoutId.None);
    ObjectUtils.copyProps(data, newWorkout);
    return newWorkout;
  }
}
