export class Exercise {
  public id: number;
  public name: string;
  public sets: string;
  public rest: number;
  public reps: string;
  public weight: number;
}
