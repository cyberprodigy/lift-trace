import { Workout } from "./Workout";
import { IOCContainer } from "../lifeCycle/IOCContainer";
import { ResourceId } from "../persistence/PersistentCache";
import { ObjectUtils } from "../utils/ObjectUtils";

export class WorkoutModel implements ISerializable {
  private CurrentWorkoutId: WorkoutId = WorkoutId.None;
  private Workouts = new Array<Workout>();

  constructor() {}

  public save(): void {
    console.log("Saving model");
    IOCContainer.getInstance().persistenceCache.save(
      ResourceId.WORKOUT_MODEL,
      this
    );
  }

  public load(): void {
    var data: string = IOCContainer.getInstance().persistenceCache.load(
      ResourceId.WORKOUT_MODEL
    );
    var success: boolean = this.parse(data);
    if (!success) {
      this.loadDefaults();
    }

    console.log("Loading model WasSuccess: " + success);
  }

  public loadDefaults(): void {
    // Upper workout
    var upperWorkout: Workout = new Workout(WorkoutId.Upper);
    upperWorkout.addExcercise(0, "Bench press", "4", 180, "4-6", 45);
    upperWorkout.addExcercise(1, "Seated row", "3", 120, "6-8", 60);
    upperWorkout.addExcercise(
      2,
      "Standing overhead press",
      "3",
      150,
      "8-10",
      10
    );
    upperWorkout.addExcercise(3, "Lat pulldown", "3", 120, "10-12", 50);
    upperWorkout.addExcercise(
      4,
      "High to low cable flies",
      "3",
      90,
      "12-15",
      12
    );
    upperWorkout.addExcercise(5, "Laying facepulls", "3", 90, "10-12", 30);

    // Lower body 1 workout
    var lower1Workout: Workout = new Workout(WorkoutId.Lower1);
    lower1Workout.addExcercise(0, "Quad machine", "3", 60, "10-12", 32); //10 - 10 - 10
    lower1Workout.addExcercise(1, "Romanian deadlift", "3", 150, "6-8", 70); //8 - 8 - 8
    lower1Workout.addExcercise(2, "Calf raise", "3", 90, "25", 100); //8 - 8 - 8
    lower1Workout.addExcercise(3, "Leg press", "3", 120, "8-12", 90); //10 - 10 - 10

    // Push workout
    var pushWorkout: Workout = new Workout(WorkoutId.Push);
    pushWorkout.addExcercise(0, "Incline Dumbbell Press", "3", 150, "8-10", 14); //10-10-10
    pushWorkout.addExcercise(1, "Bench press", "2", 150, "6-8", 50); //7-6
    pushWorkout.addExcercise(2, "Lateral Rises", "4", 60, "8", 9); // 12-10-8-6
    pushWorkout.addExcercise(3, "Pec push machine", "2", 120, "10+", 50); // 14-10
    pushWorkout.addExcercise(
      4,
      "Overhead Rope Extensions",
      "3",
      90,
      "12-15",
      30
    ); // 15-15-15
    pushWorkout.addExcercise(5, "Bar Tricep Pushdowns", "3", 90, "8-12", 40); // 12-12-12

    // Pull workout
    var pullWorkout: Workout = new Workout(WorkoutId.Pull);
    pullWorkout.addExcercise(0, "Weighted pullups", "3", 150, "6-8", 0); // 8-5-5
    pullWorkout.addExcercise(1, "Seated row", "3", 120, "8-10", 60); //10-10-10
    pullWorkout.addExcercise(2, "Reverse pec deck", "3", 120, "10-12", 32); // 12-12-12
    pullWorkout.addExcercise(3, "Face pulls", "4", 90, "10-15", 45); // 15-12-12
    pullWorkout.addExcercise(4, "Dumbbel curls", "3", 90, "8-10", 14); // 8-8-5

    // Legs workout (Lower body 2)
    var lower2Workout: Workout = new Workout(WorkoutId.Lower2);
    lower2Workout.addExcercise(0, "Romanian Deadlift", "3", 150, "6-8", 80); //8 - 8 - 8
    lower2Workout.addExcercise(1, "Leg Press", "3", 120, "8-12", 90); //10 - 10 - 10
    lower2Workout.addExcercise(2, "Leg extensions", "3", 60, "10-15", 36); // 15-12-12
    lower2Workout.addExcercise(3, "Calf raise", "3", 90, "25", 100); //8 - 8 - 8

    this.Workouts[WorkoutId.Upper] = upperWorkout;
    this.Workouts[WorkoutId.Lower1] = lower1Workout;
    this.Workouts[WorkoutId.Push] = pushWorkout;
    this.Workouts[WorkoutId.Pull] = pullWorkout;
    this.Workouts[WorkoutId.Lower2] = lower2Workout;

    this.CurrentWorkoutId = WorkoutId.Lower1;
  }

  public setWorkout(workoutId: WorkoutId): void {
    this.CurrentWorkoutId = workoutId;
  }

  public getCurrentWorkout(): Workout {
    if (this.CurrentWorkoutId == WorkoutId.None) {
      return null;
    }

    return this.Workouts[this.CurrentWorkoutId];
  }

  public serialize(): string {
    return JSON.stringify(this);
  }

  public parse(data: string): boolean {
    var jsonData: Object = JSON.parse(data);
    ObjectUtils.copyProps(jsonData, this);

    this.Workouts = this.Workouts.map(curWorkout => {
      return Workout.fromJSON(curWorkout);
    });

    return true;
  }

  public setExcercise(excerciseId: number): void {
    this.getCurrentWorkout().setCurrentExcerciseId(excerciseId);
  }
}

export enum WorkoutId {
  Upper = 0,
  Lower1,
  Push,
  Pull,
  Lower2,
  None
}
