import document from "document";
import { ScreenId } from "../lifeCycle/ScreenId";
import { Exercise } from "../model/dto/Exercise";

export class LayoutManager {
  private allLayouts: Array<ScreenId> = [
    ScreenId.ExcerciseDetails,
    ScreenId.WorkoutSelection,
    ScreenId.LogScreen,
    ScreenId.AdjustWeight
  ];

  private _currentLayout: ScreenId;

  public showLayout(layoutId: ScreenId) {
    var that = this;
    this.allLayouts.forEach(function(curLayout: ScreenId, index: number) {
      that.hidePage(curLayout);
    });
    this.showPage(layoutId);
    this._currentLayout = layoutId;
  }

  private hidePage(screenId: ScreenId): void {
    let viewId = this.screenIdToViewId(screenId);
    let view = document.getElementById(viewId) as any;
    view.style.display = "none";
  }

  private showPage(screenId: ScreenId): void {
    let viewId = this.screenIdToViewId(screenId);
    let view = document.getElementById(viewId) as any;
    view.style.display = "inline";
  }

  public showExcercise(excercise: Exercise) {
    let title_txt = document.getElementById("title_txt");
    let sets_txt = document.getElementById("sets_txt");
    let rest_txt = document.getElementById("rest_txt");
    let reps_txt = document.getElementById("reps_txt");
    let weight_txt = document.getElementById("weight_txt");

    title_txt.text =
      excercise.id == 0 ? "&deg; " + excercise.name : excercise.name;
    sets_txt.text = excercise.sets;
    rest_txt.text = excercise.rest.toString() + "s";
    reps_txt.text = excercise.reps;
    weight_txt.text = excercise.weight.toString() + "kg";
  }

  public get currentLayout(): ScreenId {
    return this._currentLayout;
  }

  private screenIdToViewId(screenId: ScreenId): string {
    switch (screenId) {
      case ScreenId.ExcerciseDetails:
        return "excercise-details-view";

      case ScreenId.WorkoutSelection:
        return "workout-selection-view";

      case ScreenId.LogScreen:
        return "log-screen-view";

      case ScreenId.AdjustWeight:
        return "adjust-weight-view";
    }
  }
}
