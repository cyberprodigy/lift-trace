import * as fs from "fs";


export class PersistenceCache {
    constructor() {

    }

    public save(resource: ResourceId, val:ISerializable): void {
        console.log("saving " + ResourceId[resource]);
        let fileName: string = this.getResourceFileName(resource);
        let serializedData: string = val.serialize();
        
        fs.writeFileSync(fileName, serializedData, "ascii");
    };

    public load(resource:ResourceId):string
    {
        let fileName: string = this.getResourceFileName(resource);
        try{
            let serializedData: string = fs.readFileSync(fileName, "ascii");
            return serializedData;
        }
        catch
        {
            return null;
        }
        
    }

    private getResourceFileName(resourceId:ResourceId):string
    {
        return "workouts.db";
    }
}

export enum ResourceId
{
    WORKOUT_MODEL
}