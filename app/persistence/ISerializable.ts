interface ISerializable
{
    serialize():string;
    parse(data:string):boolean;
}