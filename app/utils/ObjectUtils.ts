export class ObjectUtils
{
    public static copyProps(source: any, dest: any)
    {
        for (var i in source)
        {
            if (dest.hasOwnProperty(i))
            {
                dest[i] = source[i];
            }
        }
    }
}