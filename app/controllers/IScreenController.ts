export interface IScreenController
{
    initialize(payload?:any):void;
    dispose():void;
}