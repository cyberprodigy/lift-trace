import { IScreenController } from './IScreenController';
import document from 'document';
import { IOCContainer } from '../lifeCycle/IOCContainer';
import { ScreenId } from '../lifeCycle/ScreenId';

export class WorkoutSelectionController implements IScreenController
{
    public initialize(): void {
        let list = document.getElementById("workout-list");
        let items = list.getElementsByClassName("tile-list-item");
        let model = IOCContainer.getInstance().workoutModel;

        (<any>items).forEach((element: any, index: number) => {
        let touch = element.getElementById("touch-me");
        touch.onclick = (evt: any) => {
            model.setWorkout(index);
            model.setExcercise(0);
            IOCContainer.getInstance().screenOperator.openScreen(ScreenId.ExcerciseDetails);
        }
        });
    }    
    
    public dispose(): void {
        
    }


}