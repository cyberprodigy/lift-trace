import { IScreenController } from "./IScreenController";
import document from "document";
import { IOCContainer } from "../lifeCycle/IOCContainer";
import { ScreenId } from "../lifeCycle/ScreenId";

export class AdjustWeightController implements IScreenController {
  public initialize(): void {
    let increasButton = document.getElementById("increase-weight");
    let decreaseButton = document.getElementById("decrease-weight");

    let model = IOCContainer.getInstance().workoutModel;

    this.redraw();

    increasButton.onclick = (evt: any) => {
      model.getCurrentWorkout().getCurrentExcercise().weight += 1;
      this.redraw();
    };

    decreaseButton.onclick = (evt: any) => {
      model.getCurrentWorkout().getCurrentExcercise().weight -= 1;
      this.redraw();
    };
  }

  public redraw() {
    let text = document.getElementById("adjust-weight-txt");
    let model = IOCContainer.getInstance().workoutModel;
    text.text = model.getCurrentWorkout().getCurrentExcercise().weight + "kg";
  }

  public dispose(): void {
    IOCContainer.getInstance().workoutModel.save();
  }
}
