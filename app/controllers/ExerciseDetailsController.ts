import { IScreenController } from "./IScreenController";
import { WorkoutModel, WorkoutId } from "../model/WorkoutModel";
//import { EventDispatcher, DispatchableEvent } from '../../node_modules/ts-event-dispatcher';
import { IOCContainer } from "../lifeCycle/IOCContainer";
import { ScreenId } from "../lifeCycle/ScreenId";
import document from "document";
import { ResourceId } from "../persistence/PersistentCache";
import { IKeyPressListener } from "./KeyboardController";
import { Exercise } from "../model/dto/Exercise";
import { vibration } from "haptics";

export class ExerciseDetailsController /* extends EventDispatcher*/
  implements IScreenController, IKeyPressListener {
  private timerIdx: number;
  public initialize(): void {
    var model = IOCContainer.getInstance().workoutModel;
    var layoutManager = IOCContainer.getInstance().layoutManager;

    layoutManager.showLayout(ScreenId.ExcerciseDetails);
    layoutManager.showExcercise(
      model.getCurrentWorkout().getCurrentExcercise()
    );

    let nextBtn = document.getElementById("next-btn") as any;
    nextBtn.onclick = function(evt: any) {
      console.log("Next");
      var excercise = model.getCurrentWorkout().getNextExcercise();
      layoutManager.showExcercise(excercise);
    };

    let prevBtn = document.getElementById("prev-btn") as any;
    prevBtn.onclick = function(evt: Event) {
      console.log("Previous");
      var excercise = model.getCurrentWorkout().getPreviousExcercise();
      layoutManager.showExcercise(excercise);
    };

    let startRestBtn = document.getElementById("time-section-clickarea") as any;
    startRestBtn.onclick = function(evt: Event) {
      var animation = document.getElementById("timeProgressAnimation") as any;
      var timeProgress = document.getElementById("timerProgressInstance");

      if (this.timerIdx) {
        animation.dur = 0; // finish animation
        timeProgress.animate("enable");

        clearTimeout(this.timerIdx);
        this.timerIdx = null;

        return;
      }
      var excercise = model.getCurrentWorkout().getCurrentExcercise();
      animation.dur = excercise.rest;
      timeProgress.animate("enable");

      this.timerIdx = setTimeout(() => {
        vibration.start("nudge-max");
        this.timerIdx = null;
      }, excercise.rest * 1000);
    }.bind(this);

    let adjustWeightBtn = document.getElementById("weight-clickarea") as any;
    adjustWeightBtn.onclick = function(evt: any) {
      IOCContainer.getInstance().screenOperator.openScreen(
        ScreenId.AdjustWeight
      );
    };

    IOCContainer.getInstance().keyboardController.subscribeKeyPress(this);
  }

  public onKeyPress(evt: KeyboardEvent): void {
    var key: string = evt.key;
    var model: WorkoutModel = IOCContainer.getInstance().workoutModel;

    if (key == "up") {
      model.getCurrentWorkout().getCurrentExcercise().weight += 1;
      this.redraw();
    } else if (key == "down") {
      var curExercise: Exercise = model
        .getCurrentWorkout()
        .getCurrentExcercise();
      if (curExercise.weight > 0) {
        curExercise.weight -= 1;
      }

      this.redraw();
    }
  }

  private redraw(): void {
    var model = IOCContainer.getInstance().workoutModel;
    var layoutManager = IOCContainer.getInstance().layoutManager;
    layoutManager.showExcercise(
      model.getCurrentWorkout().getCurrentExcercise()
    );
  }

  public dispose(): void {
    IOCContainer.getInstance().workoutModel.save();

    IOCContainer.getInstance().keyboardController.unsubscribeKeyPress(this);

    let nextBtn = document.getElementById("next-btn") as any;
    nextBtn.onclick = null;

    let prevBtn = document.getElementById("prev-btn") as any;
    prevBtn.onclick = null;

    let startRestBtn = document.getElementById("time-section-clickarea") as any;
    startRestBtn.onclick = null;
    // var animation = document.getElementById("timeProgressAnimation") as any;
    // var timeProgress =  document.getElementById("timerProgressInstance");
    //clearTimeout(this.timerIdx);
  }
}
