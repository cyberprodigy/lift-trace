import document from 'document';

export class KeyboardController
{
    private _listeners: Array<IKeyPressListener>;

    constructor()
    {
        this._listeners = new Array<IKeyPressListener>();
        var that = this;
        document.onkeypress = function (evt: KeyboardEvent): void {
            evt.preventDefault();
            that._listeners.forEach(curListener => {curListener.onKeyPress(evt)});
        }
        
    }

    public subscribeKeyPress(target: IKeyPressListener)
    {
        this._listeners.push(target);
    }

    public unsubscribeKeyPress(target: IKeyPressListener)
    {
        this._listeners.splice(this._listeners.indexOf(target), 1);
    }
}

export interface IKeyPressListener
{
    onKeyPress(evt: KeyboardEvent): void;
}