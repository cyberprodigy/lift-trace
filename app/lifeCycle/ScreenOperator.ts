import { ScreenId } from "./ScreenId";
import { LayoutManager } from "../view/LayoutManager";
import { IScreenController } from "../controllers/IScreenController";
import { WorkoutSelectionController } from "../controllers/WorkoutSelectionController";
import { ExerciseDetailsController } from "../controllers/ExerciseDetailsController";
import document from "document";
import { me } from "appbit";
import { IOCContainer } from "./IOCContainer";
import { ResourceId, PersistenceCache } from "../persistence/PersistentCache";
import { WorkoutModel, WorkoutId } from "../model/WorkoutModel";
import { IKeyPressListener } from "../controllers/KeyboardController";
import { AdjustWeightController } from "../controllers/AdjustWeightController";

export class ScreenOperator implements IKeyPressListener {
  private _currentOpenedScreen: IScreenController;

  public initialize() {
    var model: WorkoutModel = IOCContainer.getInstance().workoutModel;
    model.load();

    if (model.getCurrentWorkout()) {
      this.openScreen(ScreenId.ExcerciseDetails);
    } else {
      this.openScreen(ScreenId.WorkoutSelection);
    }

    var that = this;

    IOCContainer.getInstance().keyboardController.subscribeKeyPress(this);
  }

  public onKeyPress(evt: KeyboardEvent): void {
    console.log("ScreenOperator got the event");

    if (evt.key == "back") {
      if (
        IOCContainer.getInstance().layoutManager.currentLayout ==
        ScreenId.WorkoutSelection
      ) {
        me.exit();
      } else if (
        IOCContainer.getInstance().layoutManager.currentLayout ==
        ScreenId.AdjustWeight
      ) {
        IOCContainer.getInstance().screenOperator.openScreen(
          ScreenId.ExcerciseDetails
        );
      } else {
        IOCContainer.getInstance().screenOperator.openScreen(
          ScreenId.WorkoutSelection
        );
      }
    }
  }

  public openScreen(screenId: ScreenId, payload?: any): void {
    IOCContainer.getInstance().layoutManager.showLayout(screenId);
    if (this._currentOpenedScreen != null) {
      this._currentOpenedScreen.dispose();
    }

    this._currentOpenedScreen = this.createController(screenId);
    this._currentOpenedScreen.initialize(payload);
  }
  private createController(screenId: ScreenId): IScreenController {
    console.log("createController");
    switch (screenId) {
      case ScreenId.WorkoutSelection:
        return new WorkoutSelectionController();
      case ScreenId.ExcerciseDetails:
        return new ExerciseDetailsController();
      case ScreenId.AdjustWeight:
        return new AdjustWeightController();
    }
  }
}
