import { LayoutManager } from "../view/LayoutManager";
import { PersistenceCache } from '../persistence/PersistentCache';
import { ScreenOperator } from './ScreenOperator';
import { WorkoutModel } from '../model/WorkoutModel';
import { KeyboardController } from '../controllers/KeyboardController';

export class IOCContainer
{
    public layoutManager:LayoutManager;
    public persistenceCache:PersistenceCache;
    public screenOperator:ScreenOperator;
    public workoutModel: WorkoutModel;
    public keyboardController: KeyboardController;
    private static _instance: IOCContainer;
    

    private constructor()
    {
        this.layoutManager = new LayoutManager();
        this.persistenceCache = new PersistenceCache();
        this.screenOperator = new ScreenOperator();
        this.workoutModel = new WorkoutModel();
        this.keyboardController = new KeyboardController();
    }

    public static getInstance():IOCContainer
    {
        if(!this._instance)
        {
            this._instance = new IOCContainer();
        }
        return this._instance;
    }
}