export enum ScreenId {
  AdjustWeight = "AdjustWeight",
  ExcerciseDetails = "ExcerciseDetails",
  WorkoutSelection = "WorkoutSelection",
  LogScreen = "LogScreen"
}
